import java.util.Scanner;
//Сделать программу для симметричного шифрования//дешифровки
//Симметричная - значит, что для шифрования//дешифровки применяется один ключ.
//Алгоритм придумайте сами.
//Программа получает на вход сообщение для (зашифровки или дешифровки (одна строка)) и ключ (одна строка)
//Программа выдает зашифрованное или расшифрованное предложение.
//Необходимо сделать проект со структурой (классы Encoder // Decoder как минимум)
//Желательно получить эффект лавины. То есть при минимальном изменении сообщения или ключа результат шифрования должен сильно отличаться.

public class Encryption {
    public String alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";

    public String makeAlphabet(String key) {
        String newAlphabet = key + alphabet;
        char symbol = ' ';
        String stemp = "";

        for (int i = 0; i < newAlphabet.length(); i++) {
            symbol = newAlphabet.charAt(i);
            stemp = String.valueOf(symbol);
            newAlphabet = newAlphabet.replaceAll(stemp, "-");
            newAlphabet = newAlphabet.replaceFirst("-", stemp);
            newAlphabet = newAlphabet.replaceAll("-", "");
        }

        return newAlphabet;
    }


    public String encoding(String withoutDoubleNewAlphabet, String text) {
        char newText[] = new char[text.length()];
        char[] arrayText = text.toCharArray();
        for (int i = 0; i < text.length(); i++) {
            char symbol = arrayText[i];
            newText[i] = '-';
            if (withoutDoubleNewAlphabet.indexOf(symbol) == -1) {
                newText[i] = symbol;
            } else {
                int index = alphabet.indexOf(symbol);
                newText[i] = withoutDoubleNewAlphabet.charAt(index);
            }
        }
        String sNewText = "";
        return sNewText.copyValueOf(newText);
    }

    public String decoding(String withoutDoubnewAlphabet, String text) {

        char uncodedText[] = new char[text.length()];
        char cArAlf[] = alphabet.toCharArray();
        char[] arrayText = text.toCharArray();
        for (int i = 0; i < text.length(); i++) {
            char symbol = arrayText[i];
            uncodedText[i] = '-';
            if (withoutDoubnewAlphabet.indexOf(symbol) == -1) {
                uncodedText[i] = symbol;
            } else {
                int index = withoutDoubnewAlphabet.indexOf(symbol);
                uncodedText[i] = alphabet.charAt(index);
            }
        }
        String snewText = "";
        return snewText.copyValueOf(uncodedText);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Encryption encrypter = new Encryption();


        System.out.println("Для кодирования сообщения введите \"к\", для раскодирования сообщения введите \"р\".");
        String selector = input.nextLine();
        System.out.println("Введите ключ шифрования ");
        String key = input.nextLine();
        String withoutDoubnewAlphabet = encrypter.makeAlphabet(key);
        System.out.println("Введите текст для кодирования/раскодирования ");
        String text = input.nextLine();

        if (selector.equals("к")) {
            String encodedText = encrypter.encoding(withoutDoubnewAlphabet, text);
            System.out.println(encodedText);
        } else if (selector.equals("р")) {
            String dencodedText = encrypter.decoding(withoutDoubnewAlphabet, text);
            System.out.println(dencodedText);
        } else {
            System.out.println("Некорректный ввод, попробуйте еще");
        }
    }
}



